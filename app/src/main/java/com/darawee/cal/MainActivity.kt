package com.darawee.cal

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnPlus = findViewById<Button>(R.id.btnPlus)
        btnPlus.setOnClickListener {
            val intent = Intent(MainActivity@this, PlusActivity::class.java)
            startActivity(intent)
        }

        val btnMinus = findViewById<Button>(R.id.btnMinus)
        btnMinus.setOnClickListener {
            val intent = Intent(MainActivity@this, MinusActivity::class.java)
            startActivity(intent)
        }

        val btnMultiply = findViewById<Button>(R.id.btnMultiply)
        btnMultiply.setOnClickListener {
            val intent = Intent(MainActivity@this, MultiplyActivity::class.java)
            startActivity(intent)
        }

    }

}