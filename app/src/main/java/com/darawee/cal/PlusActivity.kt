package com.darawee.cal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_plus.*
import kotlin.random.Random

class PlusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus)

        play()
        btnNext.setOnClickListener{
            play()
            txtAns.setText("Please select")
        }

        val btnBack = findViewById<Button>(R.id.btnBack)
        btnBack.setOnClickListener {
            val intent = Intent(MainActivity@this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    private fun play() {
        val random1: Int = Random.nextInt(10) + 1 //generate random numbers from 1-10
        txtNum1.setText(Integer.toString(random1)) //n1

        val random2: Int = Random.nextInt(10) + 1 //generate random numbers from 1-10
        txtNum2.setText(Integer.toString(random2)) //n2

        val sum: Int = random1 + random2
        val index: Int = Random.nextInt(until = 3) + 1
        if (index == 1) {
            btnNum1.setText(Integer.toString(sum))
            btnNum2.setText(Integer.toString(sum + 2))
            btnNum3.setText(Integer.toString(sum + 3))
        }

        if (index == 2) {
            btnNum1.setText(Integer.toString(sum - 2))
            btnNum2.setText(Integer.toString(sum))
            btnNum3.setText(Integer.toString(sum + 2))
        }

        if (index == 3) {
            btnNum1.setText(Integer.toString(sum - 4))
            btnNum2.setText(Integer.toString(sum - 2))
            btnNum3.setText(Integer.toString(sum))
        }

        btnNum1.setOnClickListener {
            if (btnNum1.text.toString().toInt() == sum) {
                txtAns.setText("Correct")
                txtCorrect.text = (txtCorrect.text.toString().toInt() + 1).toString()
            } else {
                txtAns.setText("Wrong")
                txtWrong.setText((txtWrong.text.toString().toInt() + 1).toString())
            }
        }
        btnNum2.setOnClickListener {
            if (btnNum2.text.toString().toInt() == sum) {
                txtAns.setText("Correct")
                txtCorrect.setText((txtCorrect.text.toString().toInt() + 1).toString())
            } else {
                txtAns.setText("Wrong")
                txtWrong.setText((txtWrong.text.toString().toInt() + 1).toString())
            }
        }
        btnNum3.setOnClickListener {
            if (btnNum3.text.toString().toInt() == sum) {
                txtAns.setText("Correct")
                txtCorrect.setText((txtCorrect.text.toString().toInt() + 1).toString())
            } else {
                txtAns.setText("Wrong")
                txtWrong.setText((txtWrong.text.toString().toInt() + 1).toString())
            }
        }
    }
}